
// Dependencies
var path = require('path');
var webpack = require('webpack');

// Webpack plugins
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');

// Postcss plugins
var autoprefixer = require('autoprefixer');
var postcssImport = require('postcss-import');
var postcssReporter = require('postcss-reporter');
var cssnext = require('postcss-cssnext');

function generateConfig(production) {
  var plugins = [
    new ExtractTextWebpackPlugin('[name].css', {
      allChunks: true
    })
  ];
  var output;

  if (production) {
    plugins = plugins.concat([
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false
        }
      }),
      new HtmlWebpackPlugin({
        template: 'client/index.html',
        minify: {
          removeComments: true,
          collapseWhitespace: true,
          removeRedundantAttributes: true,
          useShortDoctype: true,
          removeEmptyAttributes: true,
          removeStyleLinkTypeAttributes: true,
          keepClosingSlash: true,
          minifyJS: true,
          minifyCSS: true,
          minifyURLs: true
        },
        inject: true
      }),
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('production')
        }
      })
    ]);
  } else {
    plugins = plugins.concat([
      new HtmlWebpackPlugin({
        template: path.join(__dirname, 'client', 'index.html'),
        inject: true
      })
    ]);
  }

  return {
    entry: [
      'webpack-dev-server/client?http://localhost:8080',
      path.resolve(__dirname, 'client', 'scripts', 'app.js')
    ],
    output: {
      path: path.join(__dirname, 'build', 'public'),
      filename: 'app.js'
    },
    module: {
      loaders: [
        {
          test: /\.js$/,
          exlude: /node_modules/,
          loader: 'babel-loader?presets[]=react&presets[]=es2015'
        },
        {
          test: /\.css$/,
          loader: ExtractTextWebpackPlugin.extract('style-loader', 'css!postcss')
        },
        {
          test: /\.(jpg|png|gif|svg)$/,
          loader: 'file-loader?name=images/[name].[ext]'
        }
      ]
    },
    postcss: function() {
      return [
         autoprefixer({
           browsers: ['last 3 versions', 'IE > 8']
         }),
         postcssImport({
          glob: true,
          onImport: function (files) {
              files.forEach(this.addDependency);
          }.bind(this)
        }),
        cssnext(),
        postcssReporter({
          clearMessages: true
        })
      ];
    },
    plugins: plugins,
    debug: true,
    devtool: production ? 'source-map' : 'eval-source-map',
    target: 'web',
    progress: true
  };
}

module.exports = generateConfig(process.env.NODE_ENV === 'production');
