
// Dependencies
import fetch from 'isomorphic-fetch';
import { REQUEST_DATA, RECEIVE_DATA, HANDLE_ERROR } from '../constants/action-types';

export function requestData(request) {
  return {
    type: REQUEST_DATA,
    request
  };
};

export function receiveData(request, response) {
  return {
    type: RECEIVE_DATA,
    request,
    response
  };
};

export function handleError(request, error) {
  return {
    type: HANDLE_ERROR,
    request,
    error
  };
};

export function fetchData(request) {
  return function(dispatch) {
    dispatch(requestData(request))

    return fetch(request)
      .then(response => response.json())
      .then(json => dispatch(receiveData(request, json)))
      .catch(error => dispatch(handleError(request, error)));
  }
};
