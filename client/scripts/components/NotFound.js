
// Dependencies
import React from 'react';

export default function(props) {
  return (
    <div>
      <h1>Not Found</h1>
      <p>The page you requested could not be found.</p>
    </div>
  );
};
