
// Dependencies
import React from 'react';
import { fetchData } from '../actions/async-request';
import { connect } from 'react-redux';

const Home = ({ isFetching, response, error, onClick }) => {
  return (
    <div>
      <h1>Home</h1>
      <p>{ isFetching ? 'Loading...' : JSON.stringify(response) }</p>
      { error ? <span>{error}</span> : null }
      <button onClick={onClick}>Fetch Data</button>
    </div>
  );
};

const mapStateToProps = (state) => {
  const { asyncRequest } = state;
  const { isFetching, response, error } = asyncRequest;

  return {
    isFetching,
    response,
    error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onClick: () => {
      dispatch(fetchData('/api/test'))
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
