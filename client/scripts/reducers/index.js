
// Dependencies
import { combineReducers } from 'redux';
import { REQUEST_DATA, RECEIVE_DATA, HANDLE_ERROR } from '../constants/action-types';

function asyncRequest(state = {
  isFetching: false,
  response: null,
  error: null
}, action) {
  switch(action.type) {
    case REQUEST_DATA:
      return Object.assign({}, state, {
        isFetching: true
      });
    case RECEIVE_DATA:
      return Object.assign({}, state, {
        isFetching: false,
        response: action.response
      });
    case HANDLE_ERROR:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.error
      });
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  asyncRequest
});

export default rootReducer;
