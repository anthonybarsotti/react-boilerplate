
// Dependencies
import { Router } from 'express';

// Routes
import testRoute from './test';

// Constants
const router = Router();

router.use('/test', testRoute);

export default router;
