
// Dependencies
import apiServer from './server.api';

// Constants
const PORT = process.env.PORT || 8080;

apiServer(PORT);
