
// Dependencies
import webpack from 'webpack';
import config from '../webpack.client';
import WebpackDevServer from 'webpack-dev-server';
import apiServer from './server.api';

// Constants
const PORT = process.env.PORT || 8080;
const server = new WebpackDevServer(webpack(config), {
  quiet: true,
  historyApiFallback: true,
  proxy: {
    '/api/*': 'http://localhost:' + (PORT - 1)
  }
});

server.listen(PORT, function() {
  console.log('Dev server listening on ', PORT);
});

apiServer(PORT - 1);
