
// Dependencies
import express from 'express';
import path from 'path';
import router from './routes';

export default function(PORT) {
  var app  = express();

  app.use(express.static('public'));
  app.use('/api', router);

  app.get('*', (req, res) => {
    res.sendFile(path.join(process.cwd(), 'public', 'index.html'), (err) => {
      if (err) console.log(err);
    });
  });

  app.listen(PORT, function() {
    console.log(`API server listening on ${PORT}`);
  });
};
