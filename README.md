
React Boilerplate
=================

A full stack boilerplate for creating React apps with an Express backend. This boilerplate supports an optional backend API that can run in tandem with webpack-dev-server.

Todo
----
- Implement React and react-router
- Implement Redux
- Implement hot loading for webpack-dev-server
